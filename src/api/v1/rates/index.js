const express = require('express');

const router = express.Router();
const listRoute = require('./list');


router.use('/', listRoute);


module.exports = router;
