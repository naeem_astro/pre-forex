const API_SERVER_URL = process.env.SWAGGER_SERVER_URL

module.exports = {
  openapi: '3.0.1',
  info: {
    version: '1.3.0',
    title: 'Users',
    description: 'User management API',
    termsOfService: 'http://api_url/terms/',
    contact: {
      name: 'Wolox Team',
      email: 'hello@wolox.co',
      url: 'https://www.wolox.com.ar/'
    },
    license: {
      name: 'Apache 2.0',
      url: 'https://www.apache.org/licenses/LICENSE-2.0.html'
    }
  },
  servers: [
    {
      url: API_SERVER_URL,
      description: 'API-Server'
    }
  ],
  security: [
    {
      ApiKeyAuth: []
    }
  ],
  tags: [
    {
      name: 'CRUD operations'
    }
  ],
  paths: {
    '/api/v1/rates/latest': {
      get: {
        tags: ['Rates'],
        description: 'Get Latest Rates',
        operationId: 'getLatestRate',
        responses: {
          '200': {
            description: 'Users were obtained',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Users'
                }
              }
            }
          }
        }
      },
    },
    '/api/v1/rates': {
      get: {
        tags: ['Rates'],
        description: 'Get Rates by Duration',
        operationId: 'getRateByDuration',
        parameters: [
          {
            name: 'from',
            in: 'query',
            schema: {
              type: 'string',
              format: 'date-time',
              default: '2018-03-20T09:12:28Z'
            },
            required: true
          },
          {
            name: 'to',
            in: 'query',
            schema: {
              type: 'string',
              format: 'date-time',
              default: '2018-03-25T09:12:28Z'
            },
            required: true
          }
        ],
        responses: {
          '200': {
            description: 'Users were obtained',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Users'
                }
              }
            }
          },
          '400': {
            description: 'Missing parameters',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Error'
                },
                example: {
                  message: 'companyId is missing',
                  internal_code: 'missing_parameters'
                }
              }
            }
          }
        }
      },
    },
    '/api/v1/frequency': {
      get: {
        tags: ['Cron Frequency'],
        description: 'Get Sync Frequency',
        operationId: 'getFrequency',
        responses: {
          '200': {
            description: 'Frequency was obtained',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Frequency'
                }
              }
            }
          }
        }
      },
      post: {
        tags: ['Cron Frequency'],
        description: 'Sets Frequency',
        operationId: 'setFrequency',
        parameters: [{
          name: 'minutes',
          in: 'query',
          schema: {
            type: 'integer',
            enum: [1, 2, 3, 5, 10, 15, 30, 45],
            default: 5
          },
          required: true
        }],

        responses: {
          '200': {
            description: 'New users were created'
          },
          '400': {
            description: 'Invalid parameters',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Error'
                },
                example: {
                  message: 'User identificationNumbers 10, 20 already exist',
                  internal_code: 'invalid_parameters'
                }
              }
            }
          }
        }
      }
    }
  },
  components: {
    schemas: {
      Frequency: {
        type: 'object',
        properties: {
          minutes: {
            type: 'integer',
            description: 'Number of minutes',
            example: 10
          }
        }
      },
      identificationNumber: {
        type: 'integer',
        description: 'User identification number',
        example: 1234
      },
      username: {
        type: 'string',
        example: 'raparicio'
      },
      companyId: {
        type: 'integer',
        description: 'Company id where the user works',
        example: 15
      },
      User: {
        type: 'object',
        properties: {
          identificationNumber: {
            $ref: '#/components/schemas/identificationNumber'
          },
          username: {
            $ref: '#/components/schemas/username'
          },
          companyId: {
            $ref: '#/components/schemas/companyId'
          }
        }
      },
      Users: {
        type: 'object',
        properties: {
          users: {
            type: 'array',
            items: {
              $ref: '#/components/schemas/User'
            }
          }
        }
      },
      Error: {
        type: 'object',
        properties: {
          message: {
            type: 'string'
          },
          internal_code: {
            type: 'string'
          }
        }
      }
    },
    securitySchemes: {
      ApiKeyAuth: {
        type: 'apiKey',
        in: 'header',
        name: 'x-api-key'
      }
    }
  }
};