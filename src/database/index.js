const mongoose = require('mongoose');

function connect(mongo_db_uri, args) {
  return new Promise((resolve, reject) => {

    if (process.env.NODE_ENV === 'test') {
      const Mockgoose = require('mockgoose').Mockgoose;
      const mockgoose = new Mockgoose(mongoose);

      mockgoose.prepareStorage()
        .then(() => {
          mongoose.connect(mongo_db_uri, args)
            .then((res, err) => {
              if (err) return reject(err);
              resolve();
            })
        })
    } else {
      mongoose.connect(mongo_db_uri, args)
        .then((res, err) => {
          if (err) return reject(err);
          resolve();
        })
    }
  });
}

function close() {
  return mongoose.disconnect();
}

module.exports = { connect, close };