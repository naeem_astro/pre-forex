const cron = require("node-cron");
const axios = require('axios');
const { ARRAY_FREQUENCIES } = require('../../utils/constants');

let task = null;

const startCron = async () => {
  const durationIndex = (process.env['DURATION_CRON_INDEX']) ? parseInt(process.env['DURATION_CRON_INDEX']) : 0;
  const frequency = ARRAY_FREQUENCIES[durationIndex];
  console.log(`Running CRON for min(s):${frequency} @ ${new Date()}`);
  task = cron.schedule(`*/${frequency} * * * *`, function () {
    console.log(`Running CRON for min(s):${frequency} @ ${new Date()}`);
    axios.get('https://blockchain.info/ticker').then(response => {
      const usd = response.data.USD;
      usd['timestamp'] = new Date().getTime();
      console.log(usd)
    }).catch(err => console.log(err));
  });
  return true;
};

const stopCron = async () => {
  if (task)
    task.destroy();
};

const restartCron = async () => {
  await stopCron();
  return await startCron();
};

module.exports = {
  startCron,
  stopCron,
  restartCron
};
