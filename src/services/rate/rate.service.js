const latest = async () => {
  return { id: 1, currency: "USD", price: 4.20 };
};

const list = async (from, to) => {
  const rates = [
    { id: 1, currency: "USD", price: 4.20 },
    { id: 2, currency: "MYR", price: 142.0 },
    { id: 3, currency: "CNY", price: 24.20 }
  ];
  return { rates, from, to };
};

module.exports = {
  latest,
  list
};
